package tdd.training.bsk;

import static org.junit.Assert.*;

import org.junit.Test;


public class FrameTest {

	@Test
	public void  frameFirstThrowTest() throws BowlingException{
		
		Frame frameT = new Frame(2, 4);
		assertEquals(2, frameT.getFirstThrow());
	}
	
	@Test
	public void  frameSecondThrowTest() throws BowlingException{
		
		Frame frameT = new Frame(2, 4);
		assertEquals(4, frameT.getSecondThrow());
	}
	
	@Test
	public void  frameScoreTest() throws BowlingException{
		
		Frame frameT = new Frame(2, 6);
		assertEquals(8, frameT.getScore());
	}
	
	@Test
	public void  frameSetANDGetBonusTest() throws BowlingException{
		
		Frame frameT = new Frame(2, 6);
		frameT.setBonus(3);
		assertEquals(3, frameT.getBonus());
	}
	
	@Test
	public void  frameIsSpereTest() throws BowlingException{
		
		Frame frameT = new Frame(4, 6);
		
		assertEquals(true, frameT.isSpare());
	}
	
	@Test
	public void  frameGetScoreWithBonusBonusTest() throws BowlingException{
		
		Frame frameT = new Frame(4, 6);
		frameT.setBonus(3);
		assertEquals(13, frameT.getScore());
	}
	
	@Test
	public void  frameIsStrikeTest() throws BowlingException{
		
		Frame frameT = new Frame(10, 0);
		
		assertEquals(true, frameT.isStrike());
	}
	
	@Test
	public void  frameGetScorOfAStrikeFrameTest() throws BowlingException{
		
		Frame frameT = new Frame(10, 0);
		frameT.setBonus(4);
		assertEquals(14, frameT.getScore());
	}

}
