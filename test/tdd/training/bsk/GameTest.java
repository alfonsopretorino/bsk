package tdd.training.bsk;

import static org.junit.Assert.*;

import org.junit.Test;

public class GameTest {

	@Test
	public void gameGetFrameAtFirstThrowTesting() throws BowlingException{
		
		
		Game gameT = new Game();
		gameT.addFrame(new Frame(1, 5));
		gameT.addFrame(new Frame(3, 6));
		gameT.addFrame(new Frame(7, 2));
		gameT.addFrame(new Frame(3, 6));
		gameT.addFrame(new Frame(4, 4));
		gameT.addFrame(new Frame(5, 3));
		gameT.addFrame(new Frame(3, 3));
		gameT.addFrame(new Frame(4, 5));
		gameT.addFrame(new Frame(8, 1));
		gameT.addFrame(new Frame(2, 6));
		
		Frame expected = new Frame(1, 5);
		
		assertEquals(expected.getFirstThrow(), gameT.getFrameAt(0).getFirstThrow());
	}
	
	@Test
	public void gameGetFrameAtSecondThrowTesting() throws BowlingException{
		
		Game gameT = new Game();
		gameT.addFrame(new Frame(1, 5));
		gameT.addFrame(new Frame(3, 6));
		gameT.addFrame(new Frame(7, 2));
		gameT.addFrame(new Frame(3, 6));
		gameT.addFrame(new Frame(4, 4));
		gameT.addFrame(new Frame(5, 3));
		gameT.addFrame(new Frame(3, 3));
		gameT.addFrame(new Frame(4, 5));
		gameT.addFrame(new Frame(8, 1));
		gameT.addFrame(new Frame(2, 6));
		
		Frame expected = new Frame(1, 5);
		
		assertEquals(expected.getSecondThrow(), gameT.getFrameAt(0).getSecondThrow());
	}
	
	@Test
	public void gameScoreTesting() throws BowlingException{
		
		Game gameT = new Game();
		gameT.addFrame(new Frame(1, 5));
		gameT.addFrame(new Frame(3, 6));
		gameT.addFrame(new Frame(7, 2));
		gameT.addFrame(new Frame(3, 6));
		gameT.addFrame(new Frame(4, 4));
		gameT.addFrame(new Frame(5, 3));
		gameT.addFrame(new Frame(3, 3));
		gameT.addFrame(new Frame(4, 5));
		gameT.addFrame(new Frame(8, 1));
		gameT.addFrame(new Frame(2, 6));
		
		
		assertEquals(81, gameT.calculateScore());
	}
	
	@Test
	public void gameCalculateScoreTesting() throws BowlingException{
		
		Game gameT = new Game();
		gameT.addFrame(new Frame(1, 9));
		gameT.addFrame(new Frame(3, 6));
		gameT.addFrame(new Frame(7, 2));
		gameT.addFrame(new Frame(3, 6));
		gameT.addFrame(new Frame(4, 4));
		gameT.addFrame(new Frame(5, 3));
		gameT.addFrame(new Frame(3, 3));
		gameT.addFrame(new Frame(4, 5));
		gameT.addFrame(new Frame(8, 1));
		gameT.addFrame(new Frame(2, 6));
		
		
		assertEquals(88, gameT.calculateScore());
	}
	
	@Test
	public void gameCalculateScoreStrikeTesting() throws BowlingException{
		
		Game gameT = new Game();
		gameT.addFrame(new Frame(10, 0));
		gameT.addFrame(new Frame(3, 6));
		gameT.addFrame(new Frame(7, 2));
		gameT.addFrame(new Frame(3, 6));
		gameT.addFrame(new Frame(4, 4));
		gameT.addFrame(new Frame(5, 3));
		gameT.addFrame(new Frame(3, 3));
		gameT.addFrame(new Frame(4, 5));
		gameT.addFrame(new Frame(8, 1));
		gameT.addFrame(new Frame(2, 6));
		
		
		assertEquals(94, gameT.calculateScore());
	}
	
	@Test
	public void gameCalculateStrikeFollowedBySpareTesting() throws BowlingException{
		
		Game gameT = new Game();
		gameT.addFrame(new Frame(10, 0));
		gameT.addFrame(new Frame(4, 6));
		gameT.addFrame(new Frame(7, 2));
		gameT.addFrame(new Frame(3, 6));
		gameT.addFrame(new Frame(4, 4));
		gameT.addFrame(new Frame(5, 3));
		gameT.addFrame(new Frame(3, 3));
		gameT.addFrame(new Frame(4, 5));
		gameT.addFrame(new Frame(8, 1));
		gameT.addFrame(new Frame(2, 6));
		
		
		assertEquals(103, gameT.calculateScore());
	}
	
	@Test
	public void gameCalculateStrikeFollowedByStrikeTesting() throws BowlingException{
		
		Game gameT = new Game();
		gameT.addFrame(new Frame(10, 0));
		gameT.addFrame(new Frame(10, 0));
		gameT.addFrame(new Frame(7, 2));
		gameT.addFrame(new Frame(3, 6));
		gameT.addFrame(new Frame(4, 4));
		gameT.addFrame(new Frame(5, 3));
		gameT.addFrame(new Frame(3, 3));
		gameT.addFrame(new Frame(4, 5));
		gameT.addFrame(new Frame(8, 1));
		gameT.addFrame(new Frame(2, 6));
		
		
		assertEquals(112, gameT.calculateScore());
	}
	
	@Test
	public void gameCalculateSpareFollowedBySpareTesting() throws BowlingException{
		
		Game gameT = new Game();
		gameT.addFrame(new Frame(8, 2));
		gameT.addFrame(new Frame(5, 5));
		gameT.addFrame(new Frame(7, 2));
		gameT.addFrame(new Frame(3, 6));
		gameT.addFrame(new Frame(4, 4));
		gameT.addFrame(new Frame(5, 3));
		gameT.addFrame(new Frame(3, 3));
		gameT.addFrame(new Frame(4, 5));
		gameT.addFrame(new Frame(8, 1));
		gameT.addFrame(new Frame(2, 6));
		
		
		assertEquals(98, gameT.calculateScore());
	}
	
	@Test
	public void gameSetANDGetFirstBonusThrowTesting() throws BowlingException{
		
		Game gameT = new Game();
		gameT.addFrame(new Frame(1, 5));
		gameT.addFrame(new Frame(3, 6));
		gameT.addFrame(new Frame(7, 2));
		gameT.addFrame(new Frame(3, 6));
		gameT.addFrame(new Frame(4, 4));
		gameT.addFrame(new Frame(5, 3));
		gameT.addFrame(new Frame(3, 3));
		gameT.addFrame(new Frame(4, 5));
		gameT.addFrame(new Frame(8, 1));
		gameT.addFrame(new Frame(2, 8));
		
		gameT.setFirstBonusThrow(7);
		
		assertEquals(7, gameT.getFirstBonusThrow());
	}
	
	@Test
	public void gameCalculateSpareAsLastTesting() throws BowlingException{
		
		Game gameT = new Game();
		gameT.addFrame(new Frame(1, 5));
		gameT.addFrame(new Frame(3, 6));
		gameT.addFrame(new Frame(7, 2));
		gameT.addFrame(new Frame(3, 6));
		gameT.addFrame(new Frame(4, 4));
		gameT.addFrame(new Frame(5, 3));
		gameT.addFrame(new Frame(3, 3));
		gameT.addFrame(new Frame(4, 5));
		gameT.addFrame(new Frame(8, 1));
		gameT.addFrame(new Frame(2, 8));
		
		gameT.setFirstBonusThrow(7);
		
		assertEquals(90, gameT.calculateScore());
	}
	
	@Test
	public void gameSetANDGetSecondBonusThrowTesting() throws BowlingException{
		
		Game gameT = new Game();
		gameT.addFrame(new Frame(1, 5));
		gameT.addFrame(new Frame(3, 6));
		gameT.addFrame(new Frame(7, 2));
		gameT.addFrame(new Frame(3, 6));
		gameT.addFrame(new Frame(4, 4));
		gameT.addFrame(new Frame(5, 3));
		gameT.addFrame(new Frame(3, 3));
		gameT.addFrame(new Frame(4, 5));
		gameT.addFrame(new Frame(8, 1));
		gameT.addFrame(new Frame(2, 8));
		
		gameT.setSecondBonusThrow(2);
		
		assertEquals(2, gameT.getSecondBonusThrow());
	}
	
	@Test
	public void gameCalculateStrikeAsLastTesting() throws BowlingException{
		
		Game gameT = new Game();
		gameT.addFrame(new Frame(1, 5));
		gameT.addFrame(new Frame(3, 6));
		gameT.addFrame(new Frame(7, 2));
		gameT.addFrame(new Frame(3, 6));
		gameT.addFrame(new Frame(4, 4));
		gameT.addFrame(new Frame(5, 3));
		gameT.addFrame(new Frame(3, 3));
		gameT.addFrame(new Frame(4, 5));
		gameT.addFrame(new Frame(8, 1));
		gameT.addFrame(new Frame(10, 0));
		
		gameT.setFirstBonusThrow(7);
		gameT.setSecondBonusThrow(2);
		
		assertEquals(92, gameT.calculateScore());
	}
	
	@Test
	public void gameCalculatePerfectGameTesting() throws BowlingException{
		
		Game gameT = new Game();
		gameT.addFrame(new Frame(10, 0));
		gameT.addFrame(new Frame(10, 0));
		gameT.addFrame(new Frame(10, 0));
		gameT.addFrame(new Frame(10, 0));
		gameT.addFrame(new Frame(10, 0));
		gameT.addFrame(new Frame(10, 0));
		gameT.addFrame(new Frame(10, 0));
		gameT.addFrame(new Frame(10, 0));
		gameT.addFrame(new Frame(10, 0));
		gameT.addFrame(new Frame(10, 0));
		
		gameT.setFirstBonusThrow(10);
		gameT.setSecondBonusThrow(0);
		
		assertEquals(300, gameT.calculateScore());
	}
	

}
