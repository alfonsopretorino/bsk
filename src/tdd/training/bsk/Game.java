package tdd.training.bsk;

import java.util.ArrayList;

public class Game {

	ArrayList<Frame> iFrames;
	Frame bonusFrame;
	int firstbonusthrow = 0;
	int secondbonusthrow = 0;
	
	/**
	 * It initializes an empty bowling game.
	 */
	public Game() {
	
		iFrames = new ArrayList<>();
		
	}

	/**
	 * It adds a frame to this game.
	 * 
	 * @param frame The frame.
	 * @throws BowlingException
	 */
	public void addFrame(Frame frame) throws BowlingException {
		
		iFrames.add(frame);
	}

	/**
	 * It return the i-th frame of this game.
	 * 
	 * @param index The index (ranging from 0 to 9) of the frame.
	 * @return The i-th frame.
	 * @throws BowlingException
	 */
	public Frame getFrameAt(int index) throws BowlingException {
		
		return iFrames.get(index);	
	}

	/**
	 * It sets the first bonus throw of this game.
	 * 
	 * @param firstBonusThrow The first bonus throw.
	 * @throws BowlingException
	 */
	public void setFirstBonusThrow(int firstBonusThrow) throws BowlingException {
		this.firstbonusthrow  = firstBonusThrow;
	}

	/**
	 * It sets the second bonus throw of this game.
	 * 
	 * @param secondBonusThrow The second bonus throw.
	 * @throws BowlingException
	 */
	public void setSecondBonusThrow(int secondBonusThrow) throws BowlingException {
		this.secondbonusthrow = secondBonusThrow;
	}

	/**
	 * It returns the first bonus throw of this game.
	 * 
	 * @return The first bonus throw.
	 */
	public int getFirstBonusThrow() {
		
		return this.firstbonusthrow;
	}

	/**
	 * It returns the second bonus throw of this game.
	 * 
	 * @return The second bonus throw.
	 */
	public int getSecondBonusThrow() {
		
		return this.secondbonusthrow;
	}

	/**
	 * It returns the score of this game.
	 * 
	 * @return The score of this game.
	 * @throws BowlingException
	 */
	public int calculateScore() throws BowlingException {
		
		int totalScore = 0;
		
		for(int i = 0; i<10; i++)
		{
			if(i <= 7 && iFrames.get(i).isStrike() && iFrames.get(i+1).isStrike())
			{
				iFrames.get(i).setBonus(iFrames.get(i+1).getFirstThrow() + iFrames.get(i+2).getFirstThrow());
				totalScore = totalScore + iFrames.get(i).getScore();
			}
			else if(i<=8 && iFrames.get(i).isSpare())
			{
				iFrames.get(i).setBonus(iFrames.get(i+1).getFirstThrow());
				totalScore = totalScore + iFrames.get(i).getScore();
			}
			else if(i>=8 && iFrames.get(i).isStrike() && iFrames.get(i-1).isStrike())
			{
				iFrames.get(i).setBonus(iFrames.get(i-1).getFirstThrow() + iFrames.get(i-2).getFirstThrow());
				totalScore = totalScore + iFrames.get(i).getScore();
			}
			else if(i<=8 && iFrames.get(i).isStrike())
			{

				iFrames.get(i).setBonus(iFrames.get(i+1).getFirstThrow() + iFrames.get(i+1).getSecondThrow());
				totalScore = totalScore + iFrames.get(i).getScore();
			}
			else if(i == 9 && iFrames.get(i).isSpare())
			{
				iFrames.get(i).setBonus(firstbonusthrow);
				totalScore = totalScore + iFrames.get(i).getScore();
			}
			else if(i == 9 && iFrames.get(i).isStrike())
			{
				iFrames.get(i).setBonus(firstbonusthrow + secondbonusthrow);
				totalScore = totalScore + iFrames.get(i).getScore();
			}
			else
			{
				totalScore = totalScore + iFrames.get(i).getScore();
			}

		}
		
		return totalScore;	
	}

}
